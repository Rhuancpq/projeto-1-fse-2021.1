# projeto-1-FSE-2021.1

## Argumentos

Tanto os argumento da linha de comando quanto os dados de entrada seguem o mesmo padrão:

O primeiro argumento é: `pot` ou `TR`, sendo TR um valor numérico que representa a temperatura de referência.

O segundo argumento é:

- `0` para utilizar a chave switch
- `1` para utilizar abordagem On/Off
- `2` para utilizar abordagem PID

Caso escolha PID, os próximos argumentos serão: `kp ki kd`. De outro modo se escolher On/Off, os próximos argumentos serão: `hist`. No caso do PID todos os parâmetros devem ser fornecidos, enquanto no caso do On/Off, se a histerese não for fornecida será utilizada a histerese padrão (6 ºC).

## Experimentos

- Experimento 1 - [PID](./experiments/pid1.md)
- Experimento 2 - [PID](./experiments/pid2.md)
- Experimento 3 - [On/Off](./experiments/onoff1.md)
- Experimento 4 - [On/Off](./experiments/onoff2.md)
