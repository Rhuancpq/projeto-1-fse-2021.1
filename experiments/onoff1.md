# Experimento 3 - On/Off

# Parâmetros

- Histerese: 6 ºC

# Gráficos

## Temperaturas em função do tempo

<img src="../assets/onoff_1.svg">

## Sinal de Controle em função do tempo

<img src="../assets/control_onoff_1.svg">

# Observações

O sistema não conseguiu sequer atingir a temperatura de referência. O experimento foi conduzido na Rasp 42.
