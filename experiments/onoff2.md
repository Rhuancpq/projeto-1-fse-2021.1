# Experimento 4 - On/Off

# Parâmetros

- Histerese: 6 ºC

# Gráficos

## Temperaturas em função do tempo

<img src="../assets/onoff_2.svg">

## Sinal de Controle em função do tempo

<img src="../assets/control_onoff_2.svg">

# Observações

O experimento foi conduzido na Rasp 43.
