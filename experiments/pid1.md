# Experimento 1 - PID

# Parâmetros

- Kp: 5.0
- Ki: 1.0
- Kd: 5.0

# Gráficos

## Temperaturas em função do tempo

<img src="../assets/pid_1.svg">

## Sinal de Controle em função do tempo

<img src="../assets/control_1.svg">
