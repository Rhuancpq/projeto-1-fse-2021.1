#ifndef __CONTROL_HPP__
#define __CONTROL_HPP__

#define RESISTOR_PIN 4
#define COOLER_PIN 5
#define MIN_COOLER -40

#include <thread>
#include <wiringPi.h>
#include <softPwm.h>
#include <mutex>
#include <cmath>
#include <iostream>
using namespace std;

class Control{
public:
    void operator()(const double &control, bool & is_end, mutex &mtx);
};

#endif // __CONTROL_HPP__