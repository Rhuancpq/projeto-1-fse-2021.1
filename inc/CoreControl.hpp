#ifndef __CORE_CONTROL_HPP__
#define __CORE_CONTROL_HPP__

#include "State.hpp"
#include "OnOff.hpp"
#include <thread>
#include <mutex>
using namespace std;

extern "C" {
#include "pid.h"
}

class CoreControl {
public:
    void operator()(state & state, bool & is_end, mutex & mtx);
};

#endif