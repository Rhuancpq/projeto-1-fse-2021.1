#ifndef __DISPLAY_HPP__
#define __DISPLAY_HPP__

#include <iostream>
#include <thread>
#include <sstream>
#include <iomanip>
using namespace std;

extern "C" {
    #include "control_lcd_16x2.h"
}

class Display {
public:
    void operator()(const float & TI, const float & TE, const float & TR, bool & is_end) const;
};

#endif