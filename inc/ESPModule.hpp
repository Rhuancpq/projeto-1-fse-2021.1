#ifndef __ESPModule_hpp__
#define __ESPModule_hpp__

extern "C" {
#include "uart.h"
}

#include <thread>
#include <cmath>
#include <iostream>
using namespace std;

class ESPModule {
public:
    void operator()(int op, float & res, bool & is_end, bool & should_apply);
    void operator()(int op, bool & res, bool & is_end, bool & should_apply);
    void operator()(int op, const double & req, bool & is_end);
};

#endif