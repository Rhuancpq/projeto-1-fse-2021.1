#include <thread>
using namespace std;

extern "C" {
    #include "bme_utils.h"
}

class ExternBME {
public:
    void operator()(float & temperature, bool & is_end);

};