#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include "State.hpp"
#include <iostream>
#include <fstream>
#include <thread>
#include <sstream>
using namespace std;

class Logger {
public:
    void operator()(const state &state, bool & is_end);
};

#endif