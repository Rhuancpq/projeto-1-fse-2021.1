#ifndef __NCURSES_UTILS_HPP__
#define __NCURSES_UTILS_HPP__

#include <ncurses.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

WINDOW *create_newwin(int height, int width, int starty, int startx);

struct input_line {
    char *ln;
    int length;
    int capacity;
    int cursor;
    int last_rendered;
};

void make_buffer(struct input_line *buf);

void destroy_buffer(struct input_line *buf);

void render_line(WINDOW * win, struct input_line *buf);

int get_line_non_blocking(WINDOW * win, struct input_line *buf, char *target, int max_len);

#endif // __NCURSES_UTILS_HPP__