#ifndef __OnOff_hpp__
#define __OnOff_hpp__

void onoff_update_histeresis(double value);

void onoff_update_reference(float reference);

double onoff_control (double input);

#endif