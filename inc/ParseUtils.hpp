#ifndef __PARSEUTILS_HPP__
#define __PARSEUTILS_HPP__

#include "State.hpp"
#include <vector>
#include <string>
#include <sstream>
using namespace std;

bool parse (state &s, const vector<string> &tokens);

#endif // __PARSEUTILS_HPP__