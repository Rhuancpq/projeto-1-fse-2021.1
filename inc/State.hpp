#ifndef __STATE_HPP__
#define __STATE_HPP__

#define DEFAULT_HIST 6

#define DEFAULT_KP 5.0
#define DEFAULT_KI 1.0
#define DEFAULT_KD 5.0

// TR or pot
// 0 1 2
// on/off pid switch
// on/off hist
// pid kp ki kd
typedef struct state{
    float TR, TE, TI, hist;
    double kp, ki, kd, control;
    bool is_pot, is_pid, is_switch;
} state;

#endif // __STATE_HPP__
