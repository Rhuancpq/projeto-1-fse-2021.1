#ifndef __TEMPWINDOW_HPP__
#define __TEMPWINDOW_HPP__

#include <ncurses.h>
#include <mutex>
#include <thread>
#include "Ncurses_utils.hpp"
using namespace std;

class TempWindow {
public:
    void operator()(const float & TI, const float & TE, const float & TR, mutex & mtx, bool & is_end);

};

#endif