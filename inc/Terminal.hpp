#ifndef __TERMINAL_HPP__
#define __TERMINAL_HPP__

#include <iostream>
#include <ncurses.h>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <thread>
#include <mutex>
#include "Ncurses_utils.hpp"
#include "ParseUtils.hpp"
#include "State.hpp"
using namespace std;

class Terminal {
public:
    void operator()(mutex & mtx, state & state, bool & is_end);
};

#endif // __TERMINAL_HPP__