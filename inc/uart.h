#ifndef __UART_H__
#define __UART_H__


#ifdef __cplusplus
extern "C" {
#endif

#include "crc16.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>         //Used for UART
#include <fcntl.h>          //Used for UART
#include <termios.h> 
#include <pthread.h>

char * request_data(int type);

int send_data(int data);

#ifdef __cplusplus
}
#endif

#endif // __UART_H__