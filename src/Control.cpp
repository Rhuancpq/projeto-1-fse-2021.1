#include "Control.hpp"


void Control::operator()(const double &control, bool & is_end, mutex &mtx) {

    int current_pin = -1;

    mtx.lock();
    double prev_control = control;
    mtx.unlock();

    wiringPiSetup();
    while (!is_end) {
        mtx.lock();
        if (signbit(control) != signbit(prev_control)) {
            softPwmStop(current_pin);
            current_pin = -1;
        }

        if (control == 0) {
            // do nothing, PWM desligado
        } else if (control > 0) {
            // ativa resistor
            if(current_pin == -1) {
                current_pin = RESISTOR_PIN;
                softPwmCreate(current_pin, 0, 100);
            } 
            softPwmWrite(RESISTOR_PIN, round(control));
        } else if (control < 0) {
            // ativa cooler
            if(control < MIN_COOLER) {
                if(current_pin == -1) {
                    current_pin = COOLER_PIN;
                    softPwmCreate(current_pin, 40, 100);
                }
                softPwmWrite(COOLER_PIN, round(abs(control)));
            } 
        }

        prev_control = control;
        mtx.unlock();
        if(is_end)
            break;
        this_thread::sleep_for(300ms);
    }

    softPwmStop(COOLER_PIN);
    softPwmStop(RESISTOR_PIN);
}