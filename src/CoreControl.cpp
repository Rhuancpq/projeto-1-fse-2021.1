#include "CoreControl.hpp"

void CoreControl::operator()(state & state, bool & is_end, mutex & mtx) {
    while(!is_end) {
        if(state.is_pid){
            pid_configura_constantes(state.kp, state.ki, state.kd);
            pid_atualiza_referencia(state.TR);
            mtx.lock();
            state.control = pid_controle((double) state.TI);
            mtx.unlock();
        }else {
            onoff_update_histeresis(state.hist);
            onoff_update_reference(state.TR);
            mtx.lock();
            state.control = onoff_control(state.TI);
            mtx.unlock();
        }
        this_thread::sleep_for(300ms);
    }
}