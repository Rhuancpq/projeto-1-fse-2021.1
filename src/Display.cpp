#include "Display.hpp"

void Display::operator()(const float & TI, const float & TE, const float & TR, bool & is_end) const {
    if (wiringPiSetup() == -1) terminate();

    lcd_fd = wiringPiI2CSetup(I2C_ADDR);

    lcd_init();
    
    while(!is_end) {
        ostringstream line1;
        line1 << setprecision(2);
        line1 << fixed;
        line1 << "TI:" << showpoint << TI;
        line1 << " TE:" << showpoint << TE;

        ostringstream line2;
        line2 << setprecision(2);
        line2 << fixed;
        line2 << "TR: " << showpoint << TR;

        
        lcdLoc(LINE1);
        typeln(line1.str().c_str());
        lcdLoc(LINE2);
        typeln(line2.str().c_str());

        this_thread::sleep_for(1s);
    }

    ClrLcd();
    // TODO close the LCD 
}