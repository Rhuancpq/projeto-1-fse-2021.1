#include "ESPModule.hpp"

void ESPModule::operator()(int op, float & res, bool & is_end, bool & should_apply) {
    // Requests don't need to wait too much for next cycle
    while(!is_end) {
        if(should_apply){
            char* resp = request_data(op);
            res = *((float *) resp);
        }

        this_thread::sleep_for(100ms);
    }
}

void ESPModule::operator()(int op, bool & res, bool & is_end, bool & should_apply) {
    // Requests don't need to wait next cycle
    while(!is_end) {
        if(should_apply) {
            char* resp = request_data(op);
            res = (bool) *((int *) resp);
        }

        this_thread::sleep_for(100ms);
    }
}

void ESPModule::operator()(int op, const double & req, bool & is_end) {
    while(!is_end) {
        while(send_data((int) round(req)) == -1);
        
        this_thread::sleep_for(1s);
    }        
}