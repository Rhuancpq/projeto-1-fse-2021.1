#include "Logger.hpp"

void Logger::operator()(const state &state, bool & is_end){
    string filename = "log_" + to_string(time(0)) + ".csv";
    ofstream file;
    file.open(filename, ios::app);
    if(!file.is_open()){
        cout << "Erro ao abrir arquivo de log" << endl;
        return;
    }

    // escreva o cabeçalho do arquivo
    file << "Data e Hora, Temperatura interna, Temperatura externa, Temperatura de referência, Sinal de controle" << endl;


    while(!is_end){
        // escreva o estado no arquivo
        // parsed date and time
        time_t now = time(0);
        tm *ltm = localtime(&now);
        stringstream ss;
        ss << ltm->tm_mday << "/" << 1 + ltm->tm_mon << "/" << 1900 + ltm->tm_year << " " << ltm->tm_hour << ":" << ltm->tm_min << ":" << ltm->tm_sec;
        file << ss.str() << "," << state.TI 
        << "," << state.TE << "," << state.TR 
        << "," << state.control << endl;
        this_thread::sleep_for(2s);
    }

    file.close();
}