#include "OnOff.hpp"

bool is_in_range = false;
double referece_value = 0;
double histeresis = 0;
double last_control = 0;

bool check_range(double value){
    return value < referece_value + histeresis && 
    value > referece_value - histeresis;
}

void onoff_update_histeresis(double value){
    histeresis = value/2;
}

void onoff_update_reference(float reference) {
    referece_value = (double) reference;
}

double onoff_control (double input){
    is_in_range = check_range(input);
    if (is_in_range) {
        return last_control;
    } else {
        if (input > referece_value) {
            last_control = -100;
        } else if (input < referece_value) {
            last_control = 100;
        }
        return last_control;   
    }
}