#include "ParseUtils.hpp"

bool parse (state &state, const vector<string> &tokens){
    bool is_error = false;
    int argc = tokens.size();
    if (argc >= 1) {
        if (tokens[0] == "pot") {
            state.is_pot = true;
        } else {
            stringstream ss(tokens[0]);
            ss >> state.TR;
            if (ss.fail()) {
                is_error = true;
                state.is_pot = true;
                state.TR = 0;
                return is_error;
            }else {
                state.is_pot = false;
            }
            state.TR = max(state.TE, state.TR);
        }
    }

    if (!is_error && argc >= 2) {
        stringstream ss(tokens[1]);
        int m;
        ss >> m;
        if (ss.fail()) {
            is_error = true;
            m = -1;
        } 
        switch (m)
        {
        case 0:
            state.is_switch = true;
            break;
        case 1:
            state.is_pid = false;
            state.is_switch = false;
            break;
        case 2:
            state.is_pid = true;
            state.is_switch = false;
            break;
        default:
            state.is_switch = true;
            break;
        }
    } else {
        state.is_switch = true;
    }

    if (is_error)
        return is_error;

    // On Off mode
    if (!is_error && !state.is_switch && !state.is_pid) {
        if(argc >= 3){
            stringstream ss(tokens[2]);
            ss >> state.hist;
            if(ss.fail()){
                is_error = true;
                state.hist = DEFAULT_HIST;
            }
        }else {
            state.hist = DEFAULT_HIST;
        }
    } else {
        if(!is_error && !state.is_switch && argc >= 5) {
            try{
                state.kp = stod(tokens[2]);
            }
            catch(const std::invalid_argument& e) {
                state.kp = DEFAULT_KP;
            }

            try{
                state.ki = stod(tokens[3]);
            }
            catch(const std::invalid_argument& e) {
                state.ki = DEFAULT_KI;
            }

            try{
                state.kd = stod(tokens[4]);
            }
            catch(const std::invalid_argument& e) {
                state.kd = DEFAULT_KD;
            }
        }else {
            state.kp = DEFAULT_KP;
            state.ki = DEFAULT_KI;
            state.kd = DEFAULT_KD;
        }
    }

    return is_error;
}