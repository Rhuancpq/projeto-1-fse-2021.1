#include "TempWindow.hpp"

void TempWindow::operator()(const float & TI, const float & TE, const float & TR, mutex & mtx, bool & is_end){
    int row, col;
    mtx.lock();
    getmaxyx(stdscr, row, col);
    
    int w1_height = 7;
    int w1_width = 50;

    WINDOW *temp_win = create_newwin(w1_height, w1_width, 5, col/2 - w1_width/2);
    mtx.unlock();

    int info_1 = 2;
    int info_2 = 3;
    int info_3 = 4;
    int header = 0;
    int padding_left = 1;
    int padding = 3;

    while(!is_end){
        mtx.lock();
        wmove(temp_win, info_1, padding);
        wclrtoeol(temp_win);
        wmove(temp_win, info_2, padding);
        wclrtoeol(temp_win);
        wmove(temp_win, info_3, padding);
        wclrtoeol(temp_win);
        mvwprintw(temp_win, info_1, padding, "Temperatura de Referencia: %.2f ºC", TR);
        mvwprintw(temp_win, info_2, padding, "Temperatura Externa: %.2f ºC", TE);
        mvwprintw(temp_win, info_3, padding, "Temperatura Interna: %.2f ºC", TI);
        box(temp_win, 0, 0);    
        mvwprintw(temp_win, header, padding_left, "Temperaturas");
        wrefresh(temp_win);
        mtx.unlock();

        this_thread::sleep_for(900ms);
    }

    delwin(temp_win);
}