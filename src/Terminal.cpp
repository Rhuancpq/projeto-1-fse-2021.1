#include "Terminal.hpp"

void Terminal::operator()(mutex & mtx, state & state, bool & is_end) {
    int row, col;
    mtx.lock();
    getmaxyx(stdscr, row, col);
    int w2_height = 9;
    int w2_width = 40;

    WINDOW *menu_win = create_newwin(w2_height, w2_width, row/2 - w2_height/2, col/2 - w2_width/2);
    wtimeout(menu_win, 0);
    wrefresh(menu_win);
    mtx.unlock();

    // No Hard coded here

    int info1_y = 1, info2_y = 2, info3_y = 3, info4_y = 4, error_y = 5, in_y = 7;
    int pad_left = 2;
    int no_pad_left = 1;

    struct input_line in_buffer;
    make_buffer(&in_buffer);

    while(!is_end){
        string info1 = "Modo: "; 
        info1 += state.is_pid ? "PID" : "On/Off";
        string info2 = "Origem:"; 
        info2 += state.is_switch ? " Switch" : ""; 
        info2 += (state.is_pot && state.is_switch) ? " | Potenciômetro." : 
        (state.is_pot? " Potenciômetro." : " Terminal.");
        stringstream info3;
        info3 << fixed;
        if(state.is_pid) {
            info3 << setprecision(1);
            info3 << "Kp: " << showpoint << state.kp 
            << " Ki: " << state.ki << " Kd: " << state.kd;
        } else {
            info3 << setprecision(2);
            info3 << "Histerese: " << showpoint << state.hist << " ºC";
        }

        mtx.lock();
        wmove(menu_win, info1_y, pad_left);
        wclrtoeol(menu_win);
        wmove(menu_win, info2_y, pad_left);
        wclrtoeol(menu_win);
        wmove(menu_win, info3_y, pad_left);
        wclrtoeol(menu_win);
        mvwprintw(menu_win, info1_y, pad_left, info1.c_str());
        mvwprintw(menu_win, info2_y, pad_left, info2.c_str());
        mvwprintw(menu_win, info3_y, pad_left, info3.str().c_str());
        wmove(menu_win, in_y, no_pad_left);
        wclrtoeol(menu_win);
        box(menu_win, 0 , 0);
        wrefresh(menu_win);
        mtx.unlock();

        //NON BLOCKING
        char ln[1024];
        int len;
        if(mtx.try_lock()){
            len = get_line_non_blocking(menu_win, &in_buffer, ln, sizeof(ln));
            render_line(menu_win, &in_buffer);
            mtx.unlock();
        }

        mtx.lock();
        wclrtoeol(menu_win);
        box(menu_win, 0 , 0);
        wmove(menu_win, in_y, no_pad_left);
        wrefresh(menu_win);
        mtx.unlock();

        if(len>0){
            vector<string> cmd;
            stringstream ss(ln);
            string item;
            while(getline(ss, item, ' ')){
                cmd.push_back(item);
            }
            
            bool is_error = parse(state, cmd);
            if(is_error){
                mtx.lock();
                mvwprintw(menu_win, error_y, pad_left, "Erro de sintaxe, fiz o que pude!");
                wrefresh(menu_win);
                mtx.unlock();
            }
        }

        this_thread::sleep_for(200ms);
    }

    destroy_buffer(&in_buffer);
    delwin(menu_win);
}