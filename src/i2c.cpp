#include "i2c.h"
#include "bme280.h"
// i2c Demo playground

int demo()   {

  if (wiringPiSetup() == -1) exit (1);

  lcd_fd = wiringPiI2CSetup(I2C_ADDR);

  //printf("lcd_fd = %d ", lcd_fd);

  lcd_init(); // setup LCD

  char array1[] = "Hello world!";

  while (1)   {

    lcdLoc(LINE1);
    typeln("Using wiringPi");
    lcdLoc(LINE2);
    typeln("Geany editor.");

    delay(2000);
    ClrLcd();
    lcdLoc(LINE1);
    typeln("I2c  Programmed");
    lcdLoc(LINE2);
    typeln("in C not Python.");

    delay(2000);
    ClrLcd();
    lcdLoc(LINE1);
    typeln("Arduino like");
    lcdLoc(LINE2);
    typeln("fast and easy.");

    delay(2000);
    ClrLcd();
    lcdLoc(LINE1);
    typeln(array1);

    delay(2000);
    ClrLcd(); // defaults LINE1
    typeln("Int  ");
    int value = 20125;
    typeInt(value);

    delay(2000);
    lcdLoc(LINE2);
    typeln("Float ");
    float FloatVal = 10045.25989;
    typeFloat(FloatVal);
    delay(2000);
  }

  return 0;

}

void leituraBME280() {
  // BME Hello world
}
