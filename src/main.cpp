#include <iostream>
#include <thread>
#include <ncurses.h>
#include <mutex>
#include <signal.h>
using namespace std;
#include "Display.hpp"
#include "ParseUtils.hpp"
#include "ESPModule.hpp"
#include "ExternBME.hpp"
#include "Terminal.hpp"
#include "TempWindow.hpp"
#include "State.hpp"
#include "Control.hpp"
#include "CoreControl.hpp"
#include "Logger.hpp"
extern "C" {
#include "uart.h"
#include "bme_utils.h"
}

bool is_end = false;

void terminate(int sig) {
    if (sig == SIGINT || sig == SIGTERM || sig == SIGQUIT) {
        is_end = true;
    }
    endwin();
    cout << "Encerrando programa..." << endl;
    sleep(2);
    cout << "Programa encerrado!" << endl;
    exit(0);
}

int main(int argc,char * argv[]) {
    signal(SIGINT, terminate);
    signal(SIGTERM, terminate);
    signal(SIGQUIT, terminate);
    state state;
    state.TR = 0;
    state.TE = 0;
    state.TI = 0;
    state.hist = DEFAULT_HIST;
    state.kd = DEFAULT_KD;
    state.kp = DEFAULT_KP;
    state.ki = DEFAULT_KI;
    state.is_pid = false;
    state.is_pot = true;
    state.is_switch = false;
    // default off
    state.control = 0;

    vector<string> tokens;
    for (int i = 1; i < argc; i++) {
        string arg = argv[i];
        tokens.push_back(arg);
    }

    bool is_error = parse(state, tokens);

    bool ALWAYS_REPLACE = true;
    

    // init ncurses
    mutex screen_mutex;
    mutex control_mutex;
    initscr();
    start_color();
    cbreak();
    nonl();               // Get return key
    keypad(stdscr, 1);    // Fix keypad
    noecho();             // No automatic printing
    curs_set(0);          // Hide real cursor
    intrflush(stdscr, 0); // Avoid potential graphical issues
    leaveok(stdscr, 1);
    init_pair(1, COLOR_CYAN, COLOR_BLACK);

    TempWindow temp_window;

    thread temp_window_thread(temp_window, cref(state.TI), cref(state.TE), cref(state.TR), ref(screen_mutex), ref(is_end));

    temp_window_thread.detach();

    Terminal terminal;

    thread terminal_thread(terminal, ref(screen_mutex), ref(state), ref(is_end));

    ESPModule int_temp;

    thread int_temp_thread(int_temp, 0, ref(state.TI), ref(is_end), ref(ALWAYS_REPLACE));

    int_temp_thread.detach();

    ESPModule pot_temp;

    thread pot_temp_thread(pot_temp, 1, ref(state.TR), ref(is_end), ref(state.is_pot));

    pot_temp_thread.detach();

    ESPModule switch_mod;

    thread switch_thread(switch_mod, 2, ref(state.is_pid), ref(is_end), ref(state.is_switch));

    switch_thread.detach();

    ESPModule control_mod;

    thread control_sig_thread(control_mod, 3, cref(state.control), ref(is_end));
    
    control_sig_thread.detach();

    ExternBME ext_temp;

    thread ext_temp_thread(ext_temp, ref(state.TE), ref(is_end));

    ext_temp_thread.detach();

    Display display;

    thread display_thread(display, cref(state.TI), cref(state.TE), cref(state.TR), ref(is_end));

    display_thread.detach();

    CoreControl core_control;

    thread core_control_thread(core_control, ref(state), ref(is_end), ref(control_mutex));

    core_control_thread.detach();

    Control control;

    thread control_thread(control, ref(state.control), ref(is_end), ref(control_mutex));

    control_thread.detach();

    Logger logger;

    thread logger_thread(logger, ref(state), ref(is_end));

    logger_thread.detach();

    terminal_thread.join();

    is_end = true;

    // FIXME: Waiting for threads to finish
    sleep(2);

    endwin();

    return 0;
}