#include "uart.h"

int uart0_filestream = -1;

pthread_mutex_t uart_mutex = PTHREAD_MUTEX_INITIALIZER;

int configure_uart(int uart0_filestream){
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;     //<Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);
    return 0;
}

int open_uart(){
    uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);      //Open in non blocking read/write mode
    
    while (uart0_filestream == -1) {
        uart0_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
    } 
    
    configure_uart(uart0_filestream);
}

int close_uart(){
    return close(uart0_filestream);
}

int send_message(int uart0_filestream, char *message, int size){
    int bytes_written = 0;
    bytes_written = write(uart0_filestream, message, size);
    return bytes_written;
}

int validate_crc_fixed(char * resp){
    char response[9];
    int byte_read = 0;
    byte_read = read(uart0_filestream, &response, 9);
    short crc = calcula_CRC(response, 7);
    short crc_received = *((short*)&response[7]);
    memcpy(resp, response+3, 4);
    return crc == crc_received;
}

/*
    type -> 0 -> Temperatura interna
    type -> 1 -> Temperatura potênciometro
    type -> 2 -> Solicita Estado da Chave On/Off
*/
char * request_data(int type){
    char message[9] = {0x01, 0x23, 0, 4, 8, 4, 8, 0, 0};
    switch (type) {
        case 0:
            message[2] = 0xC1; 
            break;

        case 1:
            message[2] = 0xC2; 
            break;

        case 2:
            message[2] = 0xC3; 
            break;
        
        default:
            break;
    }

    int flag = 0;
    char * resp = malloc(4);

    do {
        pthread_mutex_lock(&uart_mutex);
        open_uart();
        short crc = calcula_CRC(message, 7);
        memcpy(message + 7, &crc, 2);
        send_message(uart0_filestream, message, 9);


        // TODO - Fix this sleep time
        sleep(1);
        flag = validate_crc_fixed(resp);
        close_uart(uart0_filestream);
        pthread_mutex_unlock(&uart_mutex);
    } while (!flag);
    
    return resp;
}

int send_data(int data) {
    char message[13] = {0x01, 0x16, 0xD1, 4, 8, 4, 8, 0, 0, 0, 0, 0, 0};

    pthread_mutex_lock(&uart_mutex);
    open_uart();
    memcpy(message+7, &data, 4);
    short crc = calcula_CRC(message, 11);
    memcpy(message + 11, &crc, 2);
    int res = send_message(uart0_filestream, message, 13);
    close_uart(uart0_filestream);
    pthread_mutex_unlock(&uart_mutex);
    return res;
}